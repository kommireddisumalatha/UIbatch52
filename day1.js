console.log("hello world");

//var :it is used for global and functional scope
//let is a block scoped 
//const is a block scoped and fixed value

if(true){
    var a=20;
    console.log(a);
}
// Data Types
// Premetive data types
let name="javascript";
let id=101;
let coursecompletion=true;
let job=null;
let batch;

//complex data types
let scores =[34,"hello",90,55,43]
console.log(scores[1]);
console.log(scores[0]);
console.log(scores[3]);

//looping statements for loop,while loop,do while
for(let i=0;i<scores.length;i++){
    console.log(scores[i]);
}

//functions - block of reusable code
function findmax(num1,num2){
    return Math.max(num1,num2);

}
console.log(findmax(10,20));
 
//generate random number
function findrandom(num1,num2){
    return Math.random(num1,num2);
}
console.log(findrandom(3,9,12));

function randomnumber(num){
let randomnumber =math.round(math.random()*100);
console.log(math.ceil(50));
console.log(math.abs(45));
console.log(math.pow(2,5));
}


let i
let s=100;
console.log("All even numbers from 1 to 100");
for(i=1;i<=s;i++){
    if(i%2==0){
        console.log(i);
    }
}


let j
let v=100;
console.log("All odd numbers from 1 to 100");
for(j=1;j<=v;j++){
    if(j%2!==0){
        console.log(j);    
    }

}

let k
let h=50;
console.log("all even numbers from 1 to 50");
for(k=1;k<=h;k++){
    if(k%2==0){
        console.log(k);
    }
}

let x
let y=50;
console.log("all odd numbers from 1 to 50");
for(x=1;x<=y;x++){
    if(x%2!==0){
        console.log(x);
    }
}

//git init --initislizing git
//ls --to check the files
//ls -a -- to check hidden files
//git config --global user.name "sumalatha kommireddi"
//git config --global user.email "kommireddisumalatha@gmail.com"
//git add
//git comit -m "give any message"
//git status

//git remote and origin <url>
//git branch
//git push --set-upstream origin branchname
//for removing existing remote 'use git remote rm origin'